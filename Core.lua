CastSequence = CastSequence or {};

local VERSION_SETTINGS = 1;

local UPDATE_DELAY = 0.1;
local TIMEOUT_UPDATE_DELAY = 1;
local nextUpdate = 0;
local nextTimeoutUpdate = 0;

local lastInCombat = false;
local lastFriendlyTarget = 0;
local lastHostileTarget = 0;
local currentFriendlyTarget = 0;
local currentHostileTarget = 0;

local hotbarPages = {};
local pagesToHotbar = {};
local firstHotbarSlot = {};
local castingSpell = nil;

CastSequence.SystemTime = 0;
CastSequence.UniqueIdCache = {};
CastSequence.Page = {};
CastSequence.SpellWatch = {};
CastSequence.ResetWatch = {};
CastSequence.ResetWatch.Timeout = { Count = 0, Page = {} };
CastSequence.ResetWatch.InCombatChanged = { Count = 0, Page = {} };
CastSequence.ResetWatch.OnTargetChanged = { Count = 0, Page = {} };
CastSequence.IsLoaded = false;

local AdvancementType = { OnBeginCast = 1, OnCastTime = 2 };
local ActionType = { Ability = 1, Item = 2 };
local TargetType = { Friendly = 1, Hostile = 2, FriendlyOrHostile = 3 };

local sequenceAdvancement = nil;
local castingModifier = nil;
local careerLine = 0;
local careerSettings = nil;
local attachButtonQueue = {};
local isLibSlashRegistered = false;
local isLibAddonButtonRegistered = false;

local localization = CastSequence.Localization.GetMapping();

local function RegisterLibs()
	if (not isLibSlashRegistered) then
		if (LibSlash) then
			LibSlash.RegisterWSlashCmd("castsequence", function(args) CastSequence.SlashCommand(args) end);
			LibSlash.RegisterWSlashCmd("cs", function(args) CastSequence.SlashCommand(args) end);
			isLibSlashRegistered = true;
		end
	end

	if (not isLibAddonButtonRegistered) then
		if (LibAddonButton) then
			LibAddonButton.Register("fx");
			LibAddonButton.AddMenuItem("fx", "CastSequence", CastSequence.Builder.Show);
			isLibAddonButtonRegistered = true;
		end
	end
end

local function print(text)
    EA_ChatWindow.Print(towstring(tostring(text)), ChatSettings.Channels[SystemData.ChatLogFilters.SAY].id);
end

local function HasAttachedActions(sequenceSettings)

	for index, action in ipairs(sequenceSettings.Action) do
		if (action.Attached) then
			return true;
		end
	end
	
	return false;

end

local function IsTableEmpty(table)

	for _,_ in pairs(table) do
		return false;
	end
	
	return true;

end

local function SetVariable(variable, page, slot)
	
	if (not variable.Page) then
		variable.Page = {};
	end
	
	variable.Page[page] = variable.Page[page] or { Slot = {} };
	
end

local function ClearVariable(variable, page, slot)

	if (variable.Page and variable.Page[page]) then
		variable.Page[page].Slot[slot] = nil;
		if (IsTableEmpty(variable.Page[page])) then
			variable.Page[page] = nil;
		end
	end

end

local function IsHotbarPageActive(page)

	if (pagesToHotbar[page]) then
		for hotbar, _ in pairs(pagesToHotbar[page]) do
			return true;
		end
	end
	
	return false;

end

local function GetFirstHotbarSlot(bar)

	--[[
	if (not firstHotbarSlot[bar]) then		
		local firstButtonSlot = 1;
		for bar = 1, CREATED_HOTBAR_COUNT  do
			local buttonCount = ActionBarClusterSettingsManager:GetActionBarSetting(bar, "buttonCount");		
			firstHotbarSlot[bar] = firstButtonSlot;
			firstButtonSlot = firstButtonSlot + buttonCount;		
		end
	end
	
	return firstHotbarSlot[bar];
	--]]
	
	return ActionBars:GetBar(bar).m_Buttons[1].m_HotBarSlot;

end

local function GetHotbarSlots(page, slot)

	local hotbarSlots = {};

	if (pagesToHotbar[page]) then
		for hotbar, _ in pairs(pagesToHotbar[page]) do
			local firstButtonSlot = GetFirstHotbarSlot(hotbar);
			local buttonSlot = (firstButtonSlot - 1) + slot;
			table.insert(hotbarSlots, buttonSlot);
		end
	end
	
	return hotbarSlots;

end

local function IsValidSlot(page, slot)

	if (CastSequence.Page[page] == nil or CastSequence.Page[page].Slot[slot] == nil or 
		careerSettings.Page[page] == nil or careerSettings.Page[page].Slot[slot] == nil) then
		return false;
	end
	
	return true;
	
end

local function SetAttached(sequenceAction, attachButton, slotCache, fromQueue)

	if (not attachButton) then return end
	
	if (not fromQueue) then
		table.insert(attachButtonQueue, { Sequence = sequenceAction, AttachButton = attachButton, SlotCache = slotCache });
		return;
	end
	
	if (sequenceAction) then
		local action = GameData.PlayerActions.DO_ABILITY;
		local actionId = sequenceAction.Id;
		
		if (sequenceAction.Type == ActionType.Item) then
			action = GameData.PlayerActions.USE_ITEM;
			actionId = sequenceAction.UniqueId;
		end
	
		attachButton:Bind(action, actionId)
	else
		attachButton:Unbind();
	end
			
end

local function SetSlot(page, slot)

	if (not IsValidSlot(page, slot)) then return end
	
	local sequence = CastSequence.Page[page].Slot[slot];
	local index = sequence.SequenceIndex;
	local sequenceSettings = careerSettings.Page[page].Slot[slot];
	local sequenceAction = sequenceSettings.Action[index];
	
	if (not sequenceAction) then return end;
	
	local action = GameData.PlayerActions.DO_ABILITY;
	local spellId = sequenceAction.Id;
	local actionId = sequenceAction.Id;
	
	if (sequenceAction.Type == ActionType.Item) then
		action = GameData.PlayerActions.USE_ITEM;
		spellId = sequenceAction.SpellId;
		actionId = sequenceAction.UniqueId;
	end
	
	if (spellId) then	
		local spellWatch = CastSequence.SpellWatch;
		spellWatch[spellId] = spellWatch[spellId] or {};
		spellWatch[spellId][page] = spellWatch[spellId][page] or {};
		spellWatch[spellId][page][slot] = true;
	end
	
	if (IsHotbarPageActive(page)) then
		if (sequence.AttachButton) then
			SetAttached(sequenceAction.Attached, sequence.AttachButton, sequence.SlotCache);
		end
		
		for _, hotbarSlot in ipairs(sequence.SlotCache) do
			SetHotbarData(hotbarSlot, action, actionId);
		end
	end

end

local function ClearSlot(page, slot, resetHotbar)

	if (not IsValidSlot(page, slot)) then return end

	local sequence = CastSequence.Page[page].Slot[slot];
	local index = sequence.SequenceIndex;
	local sequenceSettings = careerSettings.Page[page].Slot[slot];
	local sequenceAction = sequenceSettings.Action[index];
	
	if (not sequenceAction) then return end;
	
	local spellId = sequenceAction.Id;
	
	if (sequenceAction.Type == ActionType.Item) then
		spellId = sequenceAction.SpellId;
	end

	local spellWatch = CastSequence.SpellWatch;
	if (spellWatch[spellId] and spellWatch[spellId][page]) then
		spellWatch[spellId][page][slot] = nil;
	end
	
	if (IsHotbarPageActive(page)) then
		if (sequence.AttachButton) then
			SetAttached(nil, sequence.AttachButton, sequence.SlotCache);
		end
		
		for _, hotbarSlot in ipairs(sequence.SlotCache) do
			SetHotbarData(hotbarSlot, GameData.PlayerActions.NONE, 0);
		end
	end
	
end

local function ResetSlot(page, slot)

	if (not IsValidSlot(page, slot)) then return end
	
	local sequenceSettings = careerSettings.Page[page].Slot[slot];
	local sequence = CastSequence.Page[page].Slot[slot];
	
	if (sequence.SequenceIndex ~= 1) then
		ClearSlot(page, slot);
		sequence.SequenceIndex = 1;
		SetSlot(page, slot);
	end
			
end

local function SetNextSlot(page, slot)

	if (not IsValidSlot(page, slot)) then return end

	ClearSlot(page, slot);

	local sequenceSettings = careerSettings.Page[page].Slot[slot];
	local sequence = CastSequence.Page[page].Slot[slot];
	local index = (sequence.SequenceIndex % #sequenceSettings.Action) + 1;
	sequence.SequenceIndex = index;
	sequence.LastUse = CastSequence.SystemTime;
	
	SetSlot(page, slot);

end

function CastSequence.Initialize()
	RegisterEventHandler(SystemData.Events.LOADING_END, "CastSequence.OnLoadingEnd");
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "CastSequence.OnLoadingEnd");
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "CastSequence.OnBeginCast");
	RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "CastSequence.OnEndCast");
	RegisterEventHandler(SystemData.Events.PLAYER_CAST_TIMER_SETBACK, "CastSequence.OnCastSetback");
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "CastSequence.OnTargetUpdated");
	RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_PAGE_UPDATED, "CastSequence.OnHotbarPageUpdated");
	
	print(tostring(localization["InitializedMessage"]):format("CastSequence", "/castsequence", "/cs"));
end

local function UpdateSettings()

	local version = CastSequence.Settings.Version;

	-- changes from version to version go here
	if (version == nil or type(version) == "string") then
		version = 1;
	end
	
	if (version == 1) then
		version = 2;
	end
			
end

function CastSequence.LoadSettings()

	if (not CastSequence.Settings) then
		CastSequence.Settings = {};
	else
		UpdateSettings();
	end
	
	local settings = CastSequence.Settings;	
	settings.Version = VERSION_SETTINGS;
	
	if (not settings.SequenceAdvancement) then
		settings.SequenceAdvancement = AdvancementType.OnCastTime;
	end
	
	if (not settings.CastingModifier) then
		settings.CastingModifier = 1;
	end
	
	if (not settings.Career) then
		settings.Career = {};
	end
	
	if (not settings.Career[careerLine]) then
		settings.Career[careerLine] = {};
	end
	
	careerSettings = settings.Career[careerLine];
	
	if (not careerSettings.Page) then
		careerSettings.Page = {};
	end
	
	for page, pageData in pairs(careerSettings.Page) do
		local isEmpty = true;
		for slot, slotData in pairs(pageData.Slot) do
			isEmpty = false;
			CastSequence.RegisterSequence(page, slot);
		end
		-- cleanup empty settings
		if (isEmpty) then
			careerSettings.Page[page] = nil;
		end
	end
	
	CastSequence.OnSettingsChanged();

end

function CastSequence.SlashCommand(args)

	CastSequence.Builder.Show();

end

function CastSequence.OnLoadingEnd()

	if (CastSequence.IsLoaded) then return end
	CastSequence.IsLoaded = true;
	
	RegisterLibs();

	careerLine = GameData.Player.career.line;

	currentFriendlyTarget = GameData.Player.worldObjNum;
	lastFriendlyTarget = currentFriendlyTarget;
	
	lastInCombat = GameData.Player.inCombat;
	
	CastSequence.LoadSettings();
	
	CastSequence.Setup.Initialize();
	CastSequence.Builder.Initialize();
	CastSequence.SequenceBuilder.Initialize();

end

function CastSequence.OnSettingsChanged()

	local settings = CastSequence.Settings;
	sequenceAdvancement = settings.SequenceAdvancement;
	castingModifier = settings.CastingModifier;

end

local function UpdateSpellCasted()

	if (not castingSpell) then return end
	
	local spellId = castingSpell.SpellId;
	local spellWatch = CastSequence.SpellWatch;
	
	if (spellWatch[spellId]) then
		for page, pageData in pairs(spellWatch[spellId]) do
			for slot, _ in pairs(pageData) do
				SetNextSlot(page, slot);
			end
		end
	end

end

function CastSequence.OnBeginCast(spellId, channel, castTime)

	if (castingSpell) then	
		-- spell casting can overlapping, meaning multiple OnBeginCast messages can happen
		-- this is usually caused by casting spells that are off the global cooldown
		-- if the cast is not registered in a sequence, it's cast will be ignored, else it 
		-- will overwrite the old cast
		local spellWatch = CastSequence.SpellWatch;
		
		if (not spellWatch[spellId]) then
			return;
		else -- the overlapping spell is in the spell watch
			if (spellWatch[castingSpell.SpellId]) then -- the previous spell is also being watched
				if (castTime > castingSpell.CastTime) then
					-- update the old spell early
					UpdateSpellCasted();
				else
					-- temporarily switch to the new spell, update it, then switch back
					local tempCast = castingSpell;
					castingSpell = { SpellId = spellId, Channel = channel, CastTime = castTime, StartedAt = CastSequence.SystemTime };
					UpdateSpellCasted();
					castingSpell = tempCast;
				end
			end
		end
	end

	castingSpell = { SpellId = spellId, Channel = channel, CastTime = castTime, StartedAt = CastSequence.SystemTime };
	
	if (castTime == 0 or sequenceAdvancement == AdvancementType.OnBeginCast) then
		UpdateSpellCasted();
		castingSpell = nil;
	end

end

function CastSequence.OnCastSetback(castTime)

	if (castingSpell) then
		castingSpell.CastTime = castTime;
		castingSpell.StartedAt = CastSequence.SystemTime;
	end

end

function CastSequence.OnEndCast(failed)
	
	if (castingSpell and (not failed or castingSpell.Channel)) then

		UpdateSpellCasted();
	
	end
	
	castingSpell = nil;
	
end

function CastSequence.OnTargetUpdated(targetType, targetObjectNumber, targetObjectType)

	TargetInfo:UpdateFromClient();

	if (targetType == "selfhostiletarget") then
		currentHostileTarget = targetObjectNumber;
	elseif (targetType == "selffriendlytarget") then
		if (targetObjectNumber == 0) then
			currentFriendlyTarget = GameData.Player.worldObjNum;
		else
			currentFriendlyTarget = targetObjectNumber;
		end
	end
	
end

-- physicalPage: The action bar
-- logicalPage: The page the action bar is showing
function CastSequence.OnHotbarPageUpdated(physicalPage, logicalPage)

	local lastPage = hotbarPages[physicalPage];
	hotbarPages[physicalPage] = logicalPage;
	
	if (lastPage) then
		-- remove from page to hotbar lookup
		if (pagesToHotbar[lastPage]) then
			pagesToHotbar[lastPage][physicalPage] = nil;	
		end
	end
	
	pagesToHotbar[logicalPage] = pagesToHotbar[logicalPage] or {};
	pagesToHotbar[logicalPage][physicalPage] = true;
	
	-- set any sequences that may be on the new bars
	if (CastSequence.Page[logicalPage]) then
		for slot, sequence in pairs(CastSequence.Page[logicalPage].Slot) do
			sequence.SlotCache = GetHotbarSlots(logicalPage, slot);
			
			if (sequence.AttachButton) then
				sequence.AttachButton:SetTrigger(sequence.SlotCache);
			end
	
			SetSlot(logicalPage, slot);
		end
	end

end

function CastSequence.RememberUniqueId(uniqueId, id)

	if (uniqueId and id) then
		CastSequence.UniqueIdCache[id] = uniqueId;
	end

end

function CastSequence.FindUniqueId(id)

	return CastSequence.UniqueIdCache[id];

end

local function AddResetWatch(resetWatch, page, slot, value)
	
	if (not resetWatch.Page[page] or (resetWatch.Page[page] and not resetWatch.Page[page].Slot[slot])) then
		resetWatch.Count = resetWatch.Count + 1;
	end
	
	SetVariable(resetWatch, page, slot);
	
	resetWatch.Page[page].Slot[slot] = value;
	
end

function CastSequence.RegisterSequence(page, slot)
	
	if (CastSequence.Page[page] and CastSequence.Page[page].Slot[slot]) then
		CastSequence.UnregisterSequence(page, slot);
	end

	local sequenceSettings = careerSettings.Page[page].Slot[slot];
	if (not sequenceSettings) then return end
	if (not sequenceSettings.Enabled) then return end
	if (not sequenceSettings.Reset) then
		sequenceSettings.Reset = {};
	end

	SetVariable(CastSequence, page, slot);
	
	CastSequence.Page[page].Slot[slot] = { SequenceIndex = 1, Reset = {}, SlotCache = GetHotbarSlots(page, slot) };
	local sequence = CastSequence.Page[page].Slot[slot];
	local resetWatch = CastSequence.ResetWatch;
	
	if (HasAttachedActions(sequenceSettings)) then
		sequence.AttachButton = CastSequenceAttachButton:Create(page, slot);
		sequence.AttachButton:SetTrigger(sequence.SlotCache);
	end
	
	if (sequenceSettings.Reset.Timeout) then
		AddResetWatch(resetWatch.Timeout, page, slot, true);
	end
	if (sequenceSettings.Reset.InCombatChanged) then
		AddResetWatch(resetWatch.InCombatChanged, page, slot, true);
	end
	if (sequenceSettings.Reset.Target) then
		AddResetWatch(resetWatch.OnTargetChanged, page, slot, true);
	end
	
	SetSlot(page, slot);
	
end

local function RemoveResetWatch(resetWatch, page, slot)

	if (resetWatch.Page[page] and resetWatch.Page[page].Slot[slot]) then
		ClearVariable(resetWatch, page, slot);
		resetWatch.Count = resetWatch.Count - 1;
	end

end

function CastSequence.UnregisterSequence(page, slot)

	if (not IsValidSlot(page, slot)) then return end

	local resetWatch = CastSequence.ResetWatch;
	local sequence = CastSequence.Page[page].Slot[slot];
	
	if (sequence.AttachButton) then
		sequence.AttachButton:Destroy();
		sequence.AttachButton = nil;
	end
	
	RemoveResetWatch(resetWatch.Timeout, page, slot);
	RemoveResetWatch(resetWatch.InCombatChanged, page, slot);
	RemoveResetWatch(resetWatch.OnTargetChanged, page, slot);
	
	ClearSlot(page, slot, true);
	ClearVariable(CastSequence, page, slot);

end

local function UpdateTimeout()

	for page, pageData in pairs(CastSequence.ResetWatch.Timeout.Page) do
		for slot, active in pairs(pageData.Slot) do
			if (active and IsValidSlot(page, slot)) then
				local sequenceSettings = careerSettings.Page[page].Slot[slot];
				local sequence = CastSequence.Page[page].Slot[slot];
				
				if (sequence.LastUse and sequence.LastUse ~= sequence.Reset.LastUse and (CastSequence.SystemTime - sequence.LastUse >= sequenceSettings.Reset.Timeout)) then
					sequence.Reset.LastUse = sequence.LastUse;
					ResetSlot(page, slot);
				end
			end
		end
	end

end

local function UpdateInCombatChanged()

	for page, pageData in pairs(CastSequence.ResetWatch.InCombatChanged.Page) do
		for slot, active in pairs(pageData.Slot) do
			if (active) then
				ResetSlot(page, slot);
			end
		end
	end

end

local function UpdateOnTargetChanged(friendlyChanged, hostileChanged)

	for page, pageData in pairs(CastSequence.ResetWatch.OnTargetChanged.Page) do
		for slot, active in pairs(pageData.Slot) do
			if (active) then
				local sequenceSettings = careerSettings.Page[page].Slot[slot];
				local sequence = CastSequence.Page[page].Slot[slot];
				
				if ((friendlyChanged and sequenceSettings.Reset.Target == TargetType.Friendly) or
					(hostileChanged and sequenceSettings.Reset.Target == TargetType.Hostile) or
					((friendlyChanged or hostileChanged) and sequenceSettings.Reset.Target == TargetType.FriendlyOrHostile)) then		
					
					ResetSlot(page, slot);
					
				end
			end
		end
	end

end

function CastSequence.OnUpdate(elapsed)
	CastSequence.SystemTime = CastSequence.SystemTime + elapsed;
	
	if (#attachButtonQueue > 0) then
		attachQueue = attachButtonQueue[1];
		table.remove(attachButtonQueue, 1);
		SetAttached(attachQueue.Sequence, attachQueue.AttachButton, attachQueue.AlotCache, true);
	end
	
	if (castingSpell and CastSequence.SystemTime >= castingSpell.StartedAt + math.max(castingSpell.CastTime - (castingModifier or 1), 0)) then
		UpdateSpellCasted();
		castingSpell = nil;
	end
	
	if (CastSequence.SystemTime > nextTimeoutUpdate) then
		-- since checking for timeouts is the only one that can loop through all sequences,
		-- it is seperated to only check once a second
		nextTimeoutUpdate = CastSequence.SystemTime + UPDATE_DELAY;
		if (CastSequence.ResetWatch.Timeout.Count > 0) then
			UpdateTimeout();
		end
	end
		
	if (CastSequence.SystemTime > nextUpdate) then
		nextUpdate = CastSequence.SystemTime + UPDATE_DELAY;
		-- since checking for target changes and combat change will only occur when they change,
		-- they are speed checked
		if (lastInCombat ~= GameData.Player.inCombat) then
			lastInCombat = GameData.Player.inCombat;
			if (CastSequence.ResetWatch.InCombatChanged.Count > 0) then
				UpdateInCombatChanged();
			end
		end
		if (CastSequence.ResetWatch.OnTargetChanged.Count > 0) then
			local friendlyTargetChanged = false;
			local hostileTargetChanged = false;
			if (currentFriendlyTarget ~= lastFriendlyTarget) then
				friendlyTargetChanged = true;
				lastFriendlyTarget = currentFriendlyTarget;
			end
			if (currentHostileTarget ~= lastHostileTarget) then
				hostileTargetChanged = true;
				lastHostileTarget = currentHostileTarget;
			end
			UpdateOnTargetChanged(friendlyTargetChanged, hostileTargetChanged);
		end
		
	end
end