<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="CastSequence" version="1.0.3" date="11/19/2010" >
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<Author name="Healix" email="" />
		<Description text="Add cast sequences to your action bars" />
       
		<Dependencies>
			<Dependency name="EASystem_LayoutEditor" />
		</Dependencies>

		<Files>
			<File name="Localization.lua" />
			<File name="Localization/enUS.lua" />
			<File name="Setup/Setup.lua" />
			<File name="Setup/Setup.xml" />
			<File name="Setup/Builder.lua" />
			<File name="Setup/Builder.xml" />
			<File name="Setup/SequenceBuilder.lua" />
			<File name="Setup/SequenceBuilder.xml" />
			<File name="Setup/FindAbility.lua" />
			<File name="Setup/FindAbility.xml" />
			<File name="Setup/Button.lua" />
			<File name="Setup/Button.xml" />
			<File name="Setup/LabelButton.lua" />
			<File name="Setup/LabelButton.xml" />
			<File name="Setup/AttachButton.lua" />
			<File name="Setup/AttachButton.xml" />
			<File name="Core.lua" />
		</Files>
		
		<SavedVariables>
			<SavedVariable name="CastSequence.Settings" />
		</SavedVariables>
		
		<OnInitialize>
			<CreateWindow name="CastSequenceSetupWindow" show="false" />
			<CreateWindow name="CastSequenceBuilderWindow" show="false" />
			<CreateWindow name="CastSequenceSequenceBuilderWindow" show="false" />
			<CreateWindow name="CastSequenceFindAbilityWindow" show="false" />
			<CallFunction name="CastSequence.Initialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="CastSequence.OnUpdate" />
		</OnUpdate>
		<OnShutdown/>
		<WARInfo>

		</WARInfo>
	</UiMod>
</ModuleFile>
