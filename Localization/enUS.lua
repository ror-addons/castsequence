CastSequence = CastSequence or {};
if (not CastSequence.Localization) then
	CastSequence.Localization = {};
	CastSequence.Localization.Language = {};
end

CastSequence.Localization.Language[SystemData.Settings.Language.ENGLISH] = 
{
	InitializedMessage = L"%s loaded. Type %s or %s for help.",

	["Builder.Title"] = L"Sequences",
	["Builder.SortBy.Slot"] = L"#",
	["Builder.SortBy.Slot.Tooltip"] = L"Sort by hotbar page and slot",
	["Builder.SortBy.Icon"] = L"",
	["Builder.SortBy.Icon.Tooltip"] = L"Sort by icon",
	["Builder.SortBy.Name"] = L"",
	["Builder.SortBy.Name.Tooltip"] = L"Sort by name",
	["Builder.SequenceList.Context.Remove"] = L"Remove",
	["Builder.Buttons.NewSequence"] = L"New Sequence",
	["Builder.Tooltips.Settings"] = L"Settings",
	
	["SequenceBuilder.Title"] = L"Sequence Builder",
	["SequenceBuilder.Name"] = L"Sequence Name",
	["SequenceBuilder.Page"] = L"Hotbar Page",
	["SequenceBuilder.Slot"] = L"Hotbar Slot",
	["SequenceBuilder.Enabled"] = L"Enabled",
	["SequenceBuilder.ShowHelp"] = L"Show helper",
	["SequenceBuilder.FindAbility.Tooltip"] = L"Find an ability",
	["SequenceBuilder.ResetConditions"] = L"Reset Conditions",
	["SequenceBuilder.ResetConditions.Timeout"] = L"Reset when the sequence becomes idle for",
	["SequenceBuilder.ResetConditions.Timeout.Seconds"] = L"seconds",
	["SequenceBuilder.ResetConditions.Target"] = L"Reset when your target changes",
	["SequenceBuilder.ResetConditions.Target.Friendly"] = L"Friendly",
	["SequenceBuilder.ResetConditions.Target.Hostile"] = L"Hostile",
	["SequenceBuilder.ResetConditions.Target.FriendlyOrHostile"] = L"Friendly or Hostile",
	["SequenceBuilder.ResetConditions.Combat"] = L"Reset when you enter or leave combat",
	["SequenceBuilder.ShowAttached.Tooltip"] = L"Show attached abilities",
	["SequenceBuilder.Error.MustBeBetweenNumber"] = L"Must be between %d and %d",
	["SequenceBuilder.Error.AtLeastOneActionRequired"] = L"At least 1 action is required",
	["SequenceBuilder.Error.InUse"] = L"Already in use",
	["SequenceBuilder.Error.Required"] = L"Required",
	["SequenceBuilder.Buttons.Apply"] = L"Apply Changes",
	["SequenceBuilder.Buttons.Create"] = L"Create",
	
	["Button.Tooltips.NoSpellEffect"] = L"This item has no associated spell effect",
	["Button.Tooltips.AttachEmpty"] = L"Primary slot is empty",
	
	["LabelButton.Page"] = L"Page %d",
	
	["FindAbility.Title"] = L"Find Ability",
	["FindAbility.Labels.AbilityId"] = L"Enter the ID of an ability",
	["FindAbility.Labels.ActionBar"] = L"Drag and drop the ability to use it",
	["FindAbility.Error.NotFound"] = L"Ability not found",
	
	["Setup.Title"] = L"Settings",
	["Setup.Advancement"] = L"Sequence Advancement",
	["Setup.Advancement.Info"] = L"Choose how you would like the sequence to advance",
	["Setup.AdvancementType.OnCast"] = L"On begin cast",
	["Setup.AdvancementType.OnCast.Info"] = L"When the action begins to successfully cast",
	["Setup.AdvancementType.OnCastTime"] = L"On cast time",
	["Setup.AdvancementType.OnCastTime.Info"] = L"When the action is finished casting",
	["Setup.CastModifier"] = L"Cast Time Modifier",
	["Setup.CastModifier.Info"] = L"Adjusts the cast time so that the sequence will advance before the action is finished casting",
	["Setup.CastModifier.Seconds"] = L"seconds",
	
};

