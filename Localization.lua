CastSequence = CastSequence or {};
if (not CastSequence.Localization) then
	CastSequence.Localization = {};
	CastSequence.Localization.Language = {};
end

local localizationWarning = false;

function CastSequence.Localization.GetMapping()

	local lang = CastSequence.Localization.Language[SystemData.Settings.Language.active];
	
	if (not lang) then
		if (not localizationWarning) then
			d("Your current language is not supported. English will be used instead.");
			localizationWarning = true;
		end
		lang = CastSequence.Localization.Language[SystemData.Settings.Language.ENGLISH];
	end
	
	return lang;
	
end