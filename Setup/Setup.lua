CastSequence = CastSequence or {};
CastSequence.Setup = {};
CastSequence.Setup.WindowName = "CastSequenceSetupWindow";
CastSequence.Setup.Entries = {};

local windowName = CastSequence.Setup.WindowName;
local localization = CastSequence.Localization.GetMapping();

local advancementTypeDescription = { localization["Setup.AdvancementType.OnCast.Info"], localization["Setup.AdvancementType.OnCastTime.Info"] } ;

function CastSequence.Setup.Initialize()

	LabelSetText(windowName .. "TitleLabel", localization["Setup.Title"]);
	LabelSetText(windowName .. "AdvancementLabel", localization["Setup.Advancement"]);
	LabelSetText(windowName .. "AdvancementInfoLabel", localization["Setup.Advancement.Info"]);
	LabelSetText(windowName .. "AdvancementTypeLabel", L"");
	LabelSetText(windowName .. "CastModifierLabel", localization["Setup.CastModifier"]);
	LabelSetText(windowName .. "CastModifierInfoLabel", localization["Setup.CastModifier.Info"]);
	LabelSetText(windowName .. "CastModifierSecondsLabel", localization["Setup.CastModifier.Seconds"]);
	
	ComboBoxClearMenuItems(windowName .. "AdvancementTypeComboBox");
	ComboBoxAddMenuItem(windowName .. "AdvancementTypeComboBox", localization["Setup.AdvancementType.OnCast"]);
	ComboBoxAddMenuItem(windowName .. "AdvancementTypeComboBox", localization["Setup.AdvancementType.OnCastTime"]);
	
	CastSequence.Setup.LoadSettings();
		
end

function CastSequence.Setup.LoadSettings()

	local settings = CastSequence.Settings;

	ComboBoxSetSelectedMenuItem(windowName .. "AdvancementTypeComboBox", (settings.SequenceAdvancement or 1));
	TextEditBoxSetText(windowName .. "CastModifierEditBox", towstring(settings.CastingModifier or 1))
	
	CastSequence.Setup.OnAdvancementTypeChanged();
	
end

function CastSequence.Setup.Show()

	if (WindowGetShowing(windowName)) then return end
	
	CastSequence.Setup.LoadSettings();

	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "topleft", CastSequence.Builder.WindowName, "topright", 0, 0);
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, CastSequence.Setup.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function CastSequence.Setup.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function CastSequence.Setup.OnHidden()

end

function CastSequence.Setup.OnCloseLUp()

	CastSequence.Setup.Hide();

end

function CastSequence.Setup.OnAdvancementTypeChanged()

	local value = ComboBoxGetSelectedMenuItem(windowName .. "AdvancementTypeComboBox");
	
	local description = L"";
	
	if (advancementTypeDescription[value]) then
		description = advancementTypeDescription[value];
	end
	
	LabelSetText(windowName .. "AdvancementTypeLabel", description);

	CastSequence.Settings.SequenceAdvancement = ComboBoxGetSelectedMenuItem(windowName .. "AdvancementTypeComboBox");
	CastSequence.OnSettingsChanged();

end

function CastSequence.Setup.OnCastModifierChanged()

	local castModifier = tonumber(TextEditBoxGetText(windowName .. "CastModifierEditBox"));
	CastSequence.Settings.CastingModifier = castModifier;
	CastSequence.OnSettingsChanged();

end