CastSequence = CastSequence or {};
CastSequence.FindAbility = {};
CastSequence.FindAbility.WindowName = "CastSequenceFindAbilityWindow";
	
local windowName = CastSequence.FindAbility.WindowName;
local localization = CastSequence.Localization.GetMapping();

local BUTTON_SIZE = 64;
local ACTIONBAR_PADDING = 10;

local actionbarButton = nil;
local lastAbilityId = nil;
local lastButtonData = nil;

local function GetSpellId(itemData)
	if (not itemData or not itemData.bonus) then return end
	
	for index, bonus in pairs(itemData.bonus) do
		if (bonus.reference and bonus.type == 3) then
			return bonus.reference;
		end
	end
	
	return nil;
	
end

local function CreateButtonData(item)
	
	local buttonData = {};
	buttonData.Type = item.Type;
	buttonData.Id = item.Id;
	buttonData.Icon = item.Icon;
	
	if (item.Type == ButtonType.Ability) then
		local abilityData = GetAbilityData(item.Id);
		buttonData.Name = abilityData.name;
	else
		buttonData.UniqueId = item.UniqueId;
		buttonData.SpellId = item.SpellId;
		local itemData = DataUtils.FindItem(item.UniqueId);
		
		if (itemData) then
			buttonData.Name = itemData.name;
		end
	end
	
	return buttonData;
		
end

function CastSequence.FindAbility.Initialize()

	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "topright", CastSequence.SequenceBuilder.WindowName, "topleft", 0, 0);
			
	LabelSetText(windowName .. "TitleLabel", localization["FindAbility.Title"]);
	LabelSetText(windowName .. "AbilityIdLabel", localization["FindAbility.Labels.AbilityId"]);
	LabelSetText(windowName .. "ActionBarLabel", localization["FindAbility.Labels.ActionBar"]);
	LabelSetText(windowName .. "AbilityIdEditBoxLabel", localization["FindAbility.Error.NotFound"]);
	LabelSetTextColor(windowName .. "AbilityIdEditBoxLabel", 255, 0, 0);
	WindowSetShowing(windowName .. "AbilityIdEditBoxLabel", false);
	
	local barWidth, barHeight = WindowGetDimensions(windowName .. "ActionBar");
	WindowSetDimensions(windowName .. "ActionBar", BUTTON_SIZE + ACTIONBAR_PADDING, barHeight);
	
	actionbarButton = CastSequenceButton:Create(windowName .. "ActionBarButton", 1, windowName .. "ActionBar", CastSequence.FindAbility.UpdateSlot);
	actionbarButton:AnchorTo(windowName .. "ActionBar", "topleft", "topleft", 5, 5);
	actionbarButton:SetShowing(true);
	
end

function CastSequence.FindAbility.Show()

	if (WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, CastSequence.FindAbility.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function CastSequence.FindAbility.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function CastSequence.FindAbility.OnHidden()

end

function CastSequence.FindAbility.OnCloseLUp()

	CastSequence.FindAbility.Hide();

end


function CastSequence.FindAbility.UpdateSlot(index, buttonData)

	if (not buttonData) then
		if (lastButtonData) then
			actionbarButton:SetButtonData(lastButtonData);
		end
		return;
	else
		lastButtonData = buttonData;
	end
	
	local abilityId = buttonData.Id;
	if (buttonData.Type == 2) then
		abilityId = buttonData.UniqueId;
	end
	
	local value = TextEditBoxGetText(windowName .. "AbilityIdEditBox");
	if (value ~= towstring(abilityId)) then
		lastAbilityId = abilityId;
		TextEditBoxSetText(windowName .. "AbilityIdEditBox", towstring(abilityId));
		WindowSetShowing(windowName .. "AbilityIdEditBoxLabel", false);
	end

end

function CastSequence.FindAbility.OnAbilityIdChanged()

	local abilityId = TextEditBoxGetText(windowName .. "AbilityIdEditBox");
	if (abilityId == L"") then
		WindowSetShowing(windowName .. "AbilityIdEditBoxLabel", false);
		return;
	end
	
	abilityId = tonumber(abilityId);
	if (lastAbilityId == abilityId) then return end;
	lastAbilityId = abilityId;
	
	local abilityData = GetAbilityData(abilityId);
	local buttonData = nil;
	
	if (abilityData and abilityData.id ~= 0) then		
		if (abilityData.abilityType ~= GameData.AbilityType.STANDARD and abilityData.abilityType ~= GameData.AbilityType.MORALE and 
			abilityData.abilityType ~= GameData.AbilityType.GRANTED and abilityData.abilityType ~= GameData.AbilityType.GUILD and 
			abilityData.abilityType ~= GameData.AbilityType.PET) then
			--nothing
		else
			buttonData = { Type = 1, Id = abilityData.id, Icon = abilityData.iconNum, Name = abilityData.name };
		end
	else
		local itemData = DataUtils.FindItem(abilityId);
		if (itemData) then
			buttonData = { Type = 2, Id = itemData.id, Icon = itemData.iconNum, Name = itemData.name, UniqueId = abilityId, SpellId = GetSpellId(itemData) };
		end
	end
	
	if (buttonData) then		
		lastButtonData = buttonData;
		actionbarButton:SetButtonData(buttonData);
		WindowSetShowing(windowName .. "AbilityIdEditBoxLabel", false);
	else
		lastButtonData = nil;
		if (not actionbarButton:IsEmpty()) then
			actionbarButton:SetButtonData(nil);
		end
		WindowSetShowing(windowName .. "AbilityIdEditBoxLabel", true);
	end

end
