CastSequenceAttachButton = Frame:Subclass("CastSequenceAttachButton");

function CastSequenceAttachButton:Create(page, slot)

	local buttonName = "CastSequenceAttachButtonPage" .. page .. "Slot" .. slot;
	local button = self:CreateFromTemplate(buttonName, "Root");
	button.Page = page;
	button.Slot = slot;
	button.Name = buttonName;
	button.Triggers = 0;
	button.IsBound = false;
	
	WindowSetShowing(button:GetName(), true);
	
	return button;

end

function CastSequenceAttachButton:SetTrigger(slots)

	for index = 1, 5 do -- for 5 possible action bars
		local actionId = 0;
		if (slots and slots[index]) then
			actionId = GetActionIdFromName("ACTION_BAR_" .. slots[index]);
			self.Triggers = index;
		end
		WindowSetGameActionTrigger(self:GetName() .. "Bar" .. index, actionId);
	end	

end

function CastSequenceAttachButton:Bind(action, abilityId)

	self.IsBound = true;

	for index = 1, self.Triggers do
		WindowSetGameActionData(self:GetName() .. "Bar" .. index, action, abilityId, L"")
	end	

end

function CastSequenceAttachButton:Unbind()

	if (not self.IsBound) then return end
	self.IsBound = false;

	for index = 1, self.Triggers do
		WindowSetGameActionData(self:GetName() .. "Bar" .. index, GameData.PlayerActions.NONE, 0, L"")
	end
	
end

function CastSequenceAttachButton:Destroy()

	self:SetTrigger();
	self:Unbind();
	
	FrameManager:Remove(self:GetName());
	DestroyWindow(self:GetName());

end