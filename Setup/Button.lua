CastSequenceButton = Frame:Subclass("CastSequenceButton");

local localization = CastSequence.Localization.GetMapping();
local ButtonType = { Ability = 1, Item = 2 };

local function GetSpellId(itemData)
	if (not itemData or not itemData.bonus) then return end
	
	for index, bonus in pairs(itemData.bonus) do
		if (bonus.reference and bonus.type == 3) then
			return bonus.reference;
		end
	end
	
	return nil;
	
end

local function ShowTooltip(button)

	local windowName = SystemData.ActiveWindow.name;
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, localization["Button.Tooltips.NoSpellEffect"]);
	Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "center", XOffset = 0, YOffset = -32 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
			
end

local function GetButtonData(button)
	if (button.ShowingAttached) then
		if (not button.ButtonData) then
			return nil;
		end
		return button.ButtonData.Attached;
	else
		return button.ButtonData;
	end
end

local function UpdateButton(button)
	
	local buttonData = nil;
	
	if (button.ShowingAttached) then
		if (button.ButtonData) then
			buttonData = button.ButtonData.Attached;
		end
	else
		buttonData = button.ButtonData;
	end
	
	if (buttonData) then
		CastSequence.RememberUniqueId(buttonData.UniqueId, buttonData.Id);
	
		local tex, texX, texY = GetIconData(buttonData.Icon);
		DynamicImageSetTexture(button.Icon, tex, texX, texY);
        WindowSetShowing(button.Icon, true);
		
		WindowSetShowing(button.WarningIcon, false);

		if (buttonData.Type == ButtonType.Item) then
			if (not buttonData.SpellId) then
				tex, texX, texY = GetIconData(40);
				DynamicImageSetTexture(button.WarningIcon, tex, texX, texY)
				DynamicImageSetTextureScale(button.WarningIcon, 1);
				DynamicImageSetTextureDimensions(button.WarningIcon, 30, 30);
				WindowSetShowing(button.WarningIcon, true);
				
				if (button.MouseOver) then
					ShowTooltip(button);
				end
				
			end
		end
		
	else
        WindowSetShowing(button.Icon, false);
		WindowSetShowing(button.WarningIcon, false);
	end
	
end

function CastSequenceButton:Create(buttonName, buttonIndex, parentName, updateCallback)

	local button = self:CreateFromTemplate(buttonName, parentName);
	button.Name = buttonName;
	button.Parent = parentName;
	button.Index = buttonIndex;
	button.Icon = buttonName .. "ActionIcon";
	button.WarningIcon = buttonName .. "Icon";
	button.OnUpdateCallback = updateCallback;
	
	WindowSetShowing(button.WarningIcon, false);
	
	FrameManager:ResolveWindowToFrame(buttonName .. "Action", button);
	button:ShowAttached(false);
	
	return button;

end

function CastSequenceButton:SetShowing(showing)
	if (WindowGetShowing(self:GetName()) ~= showing) then
		WindowSetShowing(self:GetName(), showing);
	end
end

function CastSequenceButton:AnchorTo(anchorFrame, pointOnAnchor, pointOnSelf, xOffset, yOffset)

	WindowClearAnchors(self:GetName());
	WindowAddAnchor(self:GetName(), pointOnAnchor, anchorFrame, pointOnSelf, xOffset, yOffset);

end

function CastSequenceButton:ShowAttached(showAttached)

	if (self.ShowingAttached == showAttached) then return end
	self.ShowingAttached = showAttached;
	
	UpdateButton(self);
	
end

function CastSequenceButton:IsEmpty()

	if (self.ButtonData and self.ShowingAttached) then
		return (self.ButtonData.Attached == nil);
	end

	return (self.ButtonData == nil);

end

function CastSequenceButton:BeginDrag()
	if (self:IsEmpty()) then return end
	
	local buttonData = GetButtonData(self);
	local action = GameData.PlayerActions.DO_ABILITY;
	local actionId = buttonData.Id;
	
	if (buttonData.Type == ButtonType.Item) then
		action = GameData.PlayerActions.USE_ITEM;
		actionId = buttonData.UniqueId;
	end
	
	Cursor.PickUp(action, 0, actionId, buttonData.Icon, Cursor.AUTO_PICKUP_ON_LBUTTON_UP);
end

function CastSequenceButton:SetAttachedButtonData(buttonData)
	
	self.ButtonData.Attached = buttonData;
	if (self.ShowingAttached) then
		UpdateButton(self);
	end

end

function CastSequenceButton:SetButtonData(buttonData)
	
	self.ButtonData = buttonData;
	UpdateButton(self);

end

function CastSequenceButton:OnLButtonDown(flags, mouseX, mouseY)
	if (not self:IsEmpty()) then
		self.DragMode = true;
	end
end

function CastSequenceButton:OnLButtonUp(flags, mouseX, mouseY)
	self.DragMode = false;
		
	if (Cursor.IconOnCursor()) then
	
		-- show tooltip if the user tries to drop an action on an attached slot that has no primary action
		if (self.ShowingAttached and not self.ButtonData) then
		
			local windowName = SystemData.ActiveWindow.name;
			Tooltips.CreateTextOnlyTooltip(windowName, nil);
			Tooltips.SetTooltipText(1, 1, localization["Button.Tooltips.AttachEmpty"]);
			Tooltips.Finalize();
			local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "center", XOffset = 0, YOffset = -32 };
			Tooltips.AnchorTooltip(anchor);
			Tooltips.SetTooltipAlpha(1);
			
			return;
		end
	
		local buttonType = nil;
		local abilityData = Player.GetAbilityData(Cursor.Data.ObjectId);
		local itemData = nil;
		local objectId = Cursor.Data.ObjectId;
		
		if (not abilityData) then
			itemData = DataUtils.FindItem(Cursor.Data.ObjectId);
			if (not itemData) then
				objectId = CastSequence.FindUniqueId(Cursor.Data.ObjectId);
				if (objectId) then
					itemData = DataUtils.FindItem(objectId);
					if (not itemData) then return end
				else
					return;
				end
			else
				CastSequence.RememberUniqueId(Cursor.Data.ObjectId, itemData.id);
			end
			buttonType = ButtonType.Item;
		else
			buttonType = ButtonType.Ability;
			if (abilityData.abilityType ~= GameData.AbilityType.STANDARD and abilityData.abilityType ~= GameData.AbilityType.MORALE and 
				abilityData.abilityType ~= GameData.AbilityType.GRANTED and abilityData.abilityType ~= GameData.AbilityType.GUILD and 
				abilityData.abilityType ~= GameData.AbilityType.PET) then return end
		end
		
		Cursor.Clear()
		
		local oldButtonData = GetButtonData(self);
		local buttonData = {};
		buttonData.Type = buttonType;
		
		if (buttonType == ButtonType.Ability) then
			buttonData.Id = abilityData.id;
			buttonData.Icon = abilityData.iconNum;
			buttonData.Name = abilityData.name;
		else
			buttonData.Id = itemData.id;
			buttonData.Icon = itemData.iconNum;
			buttonData.Name = itemData.name;
			buttonData.UniqueId = objectId;
			buttonData.SpellId = GetSpellId(itemData);
		end
		
		if (not self:IsEmpty()) then
			self:BeginDrag();
		end
		
		if (self.ShowingAttached) then
			self.ButtonData.Attached = self.ButtonData.Attached or {};
			self.ButtonData.Attached = buttonData;
			buttonData = self.ButtonData;
		elseif (oldButtonData and oldButtonData.Attached) then
			buttonData.Attached = oldButtonData.Attached;
		end
		
		self:SetButtonData(buttonData);
		self.OnUpdateCallback(self.Index, self.ButtonData);
	end
end

function CastSequenceButton:OnRButtonDown(flags, mouseX, mouseY)
	if (not self:IsEmpty()) then
		if (self.ShowingAttached) then
			self.ButtonData.Attached = nil;
		else
			self.ButtonData = nil;
		end
		self:SetButtonData(self.ButtonData);
		self.OnUpdateCallback(self.Index, self.ButtonData);
	end
end

function CastSequenceButton:OnMouseOver(flags, mouseX, mouseY)
	
	self.MouseOver = true;
	
	local buttonData = GetButtonData(self);
	if (not buttonData) then return end
	
	local anchor = { Point = "top", RelativeTo = SystemData.ActiveWindow.name, RelativePoint = "bottom", XOffset = 0, YOffset = -64 };
	
	if (buttonData.Type == ButtonType.Ability) then
		local abilityData = GetAbilityData(buttonData.Id);
		if (abilityData) then
			local text = AbilitiesWindow.AbilityTypeDesc[AbilitiesWindow.Modes.MODE_ACTION_ABILITIES];
			Tooltips.CreateAbilityTooltip(abilityData, SystemData.ActiveWindow.name, anchor, text);
		end
	else
		if (not buttonData.SpellId) then
			ShowTooltip(self);
		else
			local itemData = DataUtils.FindItem(buttonData.UniqueId);
			if (itemData) then
				Tooltips.CreateItemTooltip(itemData, SystemData.ActiveWindow.name, anchor, Tooltips.ENABLE_COMPARISON, nil, nil, nil);
			end
		end
	end

end

function CastSequenceButton:OnMouseOverEnd(flags, mouseX, mouseY)
	self.MouseOver = false;
	
	if (self.DragMode) then
		self.DragMode = false;
		self:BeginDrag();
		
		if (self.ShowingAttached) then
			self.ButtonData.Attached = nil;
		else
			self.ButtonData = nil;
		end
		self:SetButtonData(self.ButtonData);
		self.OnUpdateCallback(self.Index, self.ButtonData);
	end
end
