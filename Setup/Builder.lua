CastSequence = CastSequence or {};
CastSequence.Builder = {};
CastSequence.Builder.WindowName = "CastSequenceBuilderWindow";
CastSequence.Builder.Entries = {};

local windowName = CastSequence.Builder.WindowName;
local localization = CastSequence.Localization.GetMapping();

local careerSettings = nil;
local activeRow = nil;
local SortDirection = { Ascending = 1, Descending = 2 };
local SortType = { Slot = 1, Icon = 2, Name = 3 };

local sortDisplay = { Type = SortType.Slot, Direction = SortDirection.Ascending };
local sortButton = 
{
	[SortType.Slot] = { Name = windowName .. "SortBarSortBySlotButton", Tooltip = localization["Builder.SortBy.Slot.Tooltip"] },
	[SortType.Icon] = { Name = windowName .. "SortBarSortByIconButton", Tooltip = localization["Builder.SortBy.Icon.Tooltip"] },
	[SortType.Name] = { Name = windowName .. "SortBarSortByNameButton", Tooltip = localization["Builder.SortBy.Name.Tooltip"] }
};

local function TintRow(rowName, isActive)

	local id = WindowGetId(rowName);
	local entry = CastSequence.Builder.Entries[id];
		
	if (isActive) then
		WindowSetTintColor(rowName .. "Background", 12, 47, 158);
		WindowSetAlpha(rowName .. "Background", 0.4);
	else
		WindowSetAlpha(rowName .. "Background", 0);
	end

end

local function TintRows()
	for rowIndex, dataIndex in ipairs(CastSequenceBuilderWindowList.PopulatorIndices) do
	
		local rowName = windowName .. "ListRow" .. rowIndex;
		WindowSetId(rowName, dataIndex);
		
		local entry = CastSequence.Builder.Entries[dataIndex];
		
		local r, g, b = 255, 255, 255;
		if (not entry.Enabled) then
			r, g, b = 90, 90, 90;
		end
		
		LabelSetTextColor(rowName .. "Name", r, g, b);
		LabelSetTextColor(rowName .. "Slot", r, g, b);
		WindowSetTintColor(rowName .. "Icon", r, g, b);
	
		local tex, texX, texY = GetIconData(entry.Icon);
		DynamicImageSetTexture(rowName .. "Icon", tex, texX, texY)
		DynamicImageSetTextureDimensions(rowName .. "Icon", 64, 64);
		
		TintRow(rowName, (rowName == activeRow));
	
	end
end

local function CompareEntry(indexA, indexB)
	if (indexB == nil) then
		return false;
	end
	
	local entryA = CastSequence.Builder.Entries[indexA];
	local entryB = CastSequence.Builder.Entries[indexB];
	
	if (sortDisplay.Type == SortType.Name) then
		if (sortDisplay.Direction == SortDirection.Ascending) then
			return (WStringsCompare(entryA.Name, entryB.Name) < 0);
		else
			return (WStringsCompare(entryA.Name, entryB.Name) > 0);
		end
	elseif (sortDisplay.Type == SortType.Icon) then
		if (sortDisplay.Direction == SortDirection.Ascending) then
			return (entryA.Icon < entryB.Icon);
		else
			return (entryA.Icon > entryB.Icon);
		end
	else
		local numA = entryA.Page;
		local numB = entryB.Page;
		if (numA == numB) then
			numA = entryA.Slot;
			numB = entryB.Slot;
		end
		
		if (sortDisplay.Direction == SortDirection.Ascending) then
			return (numA < numB);
		else
			return (numA > numB);
		end
	end
	
end

local function SetupEntries()
	
	CastSequence.Builder.Entries = {};
	if (not careerSettings.Page) then return end
	
	for page, pageData in pairs(careerSettings.Page) do
		for slot, slotData in pairs(pageData.Slot) do
			local icon = 0;
			if (slotData.Action[1]) then
				icon = slotData.Action[1].Icon;
			end
			
			table.insert(CastSequence.Builder.Entries, { Id = towstring(page .. ": " .. slot), Slot = tonumber(slot), Page = tonumber(page), Name = slotData.Name, Icon = icon, Enabled = slotData.Enabled });
		end
	end
	
	CastSequence.Builder.UpdateList();
end

local function UpdateSortButtons()
    
	local type = sortDisplay.Type;
	local direction = sortDisplay.Direction;
	
	ButtonSetPressedFlag(sortButton[SortType.Slot].Name, type == SortType.Slot);
	ButtonSetPressedFlag(sortButton[SortType.Icon].Name, type == SortType.Icon);
	ButtonSetPressedFlag(sortButton[SortType.Name].Name, type == SortType.Name);
	
	WindowSetShowing(windowName .. "SortBarUpArrow", direction == SortDirection.Ascending);
	WindowSetShowing(windowName .. "SortBarDownArrow", direction == SortDirection.Descending);
	
	local window = sortButton[type];
	
	if (direction == SortDirection.Ascending) then		
		WindowClearAnchors(windowName .. "SortBarUpArrow");
		WindowAddAnchor(windowName .. "SortBarUpArrow", "right", sortButton[type].Name, "right", -10, 0);
	else
		WindowClearAnchors(windowName .. "SortBarDownArrow");
		WindowAddAnchor(windowName .. "SortBarDownArrow", "right", sortButton[type].Name, "right", -10, 0);
	end
	
end

function CastSequence.Builder.Initialize()

	careerSettings = CastSequence.Settings.Career[GameData.Player.career.line];

	LabelSetText(windowName .. "TitleLabel", localization["Builder.Title"]);
	ButtonSetText(windowName .. "NewButton", localization["Builder.Buttons.NewSequence"]);
	WindowSetShowing(windowName .. "SettingsButtonHover", false);
	
	ButtonSetText(sortButton[SortType.Slot].Name, localization["Builder.SortBy.Slot"]);
	ButtonSetText(sortButton[SortType.Icon].Name, localization["Builder.SortBy.Icon"]);
	ButtonSetText(sortButton[SortType.Name].Name, localization["Builder.SortBy.Name"]);
	
	SetupEntries();
	UpdateSortButtons();
		
end

function CastSequence.Builder.CreateContextMenu()
	
	EA_Window_ContextMenu.CreateContextMenu(activeRow);
	EA_Window_ContextMenu.AddMenuItem(localization["Builder.SequenceList.Context.Remove"], CastSequence.Builder.OnContextMenuRemoveLUp, false, true);
	EA_Window_ContextMenu.Finalize();
	
end

function CastSequence.Builder.UpdateList()

    local entriesOrdered = {};
    for index,data in ipairs(CastSequence.Builder.Entries) do
        table.insert(entriesOrdered, index);
    end

	table.sort(entriesOrdered, CompareEntry);
	ListBoxSetDisplayOrder(windowName .. "List", entriesOrdered)
	
end

function CastSequence.Builder.Show()

	if (WindowGetShowing(windowName)) then return end

	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "center", "Root", "center", 0, 0);

	local x, y = WindowGetScreenPosition(windowName);
	CastSequence.Builder.DefaultPosition = { X = x, Y = y };
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, CastSequence.Builder.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function CastSequence.Builder.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function CastSequence.Builder.OnHidden()

	CastSequence.Setup.Hide();
	CastSequence.SequenceBuilder.DestroyHelp();
	CastSequence.SequenceBuilder.Hide();

end

function CastSequence.Builder.OnCloseLUp()

	CastSequence.Builder.Hide();

end

function CastSequence.Builder.OnSettingsLClick()

	CastSequence.Setup.Show();
	
end

function CastSequence.Builder.OnSettingsOnMouseOver()
	
	WindowSetShowing(windowName .. "SettingsButtonHover", true);

	local windowName = SystemData.ActiveWindow.name;
	local type = WindowGetId(windowName);
	
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, localization["Builder.Tooltips.Settings"]);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "bottomleft", XOffset = 0, YOffset = -4 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
	
end

function CastSequence.Builder.OnSettingsOnMouseOverEnd()

	WindowSetShowing(windowName .. "SettingsButtonHover", false);
	
end

function CastSequence.Builder.OnSortButtonLUp()

	local type = WindowGetId(SystemData.ActiveWindow.name);
	
	-- if the sort type is already active, change the order.
	if (type == sortDisplay.Type) then
		if (sortDisplay.Direction == SortDirection.Ascending) then
			sortDisplay.Direction = SortDirection.Descending;
		else
			sortDisplay.Direction = SortDirection.Ascending;
		end
	else
		sortDisplay.Type = type;
		sortDisplay.Direction = SortDirection.Ascending;
	end
	
	CastSequence.Builder.UpdateList()
	UpdateSortButtons();
	
end

function CastSequence.Builder.OnSortButtonMouseOver()

	local windowName = SystemData.ActiveWindow.name;
	local type = WindowGetId(windowName);
	
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, sortButton[type].Tooltip);
	Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "center", XOffset = 0, YOffset = -32 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
	
end

function CastSequence.Builder.OnRowMouseOver()
	
	if (activeRow) then
		TintRow(activeRow, false);
	end
	
	activeRow = SystemData.MouseOverWindow.name;
	TintRow(activeRow, true);
		
end

function CastSequence.Builder.OnRowMouseOut()

	if (activeRow) then
		TintRow(activeRow, false);
		activeRow = nil;
	end
		
end

function CastSequence.Builder.OnRowLUp()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = CastSequence.Builder.Entries[id];
	if (not entry) then return end
	
	CastSequence.SequenceBuilder.ShowEdit(entry.Page, entry.Slot);
	
end

function CastSequence.Builder.OnRowRUp()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = CastSequence.Builder.Entries[id];
	if (not entry) then return end
	
	activeToRemoveItem = { Page = entry.Page, Slot = entry.Slot };
	CastSequence.Builder.CreateContextMenu();
		
end

function CastSequence.Builder.OnContextMenuRemoveLUp()
	
	if (activeToRemoveItem) then		
		if (careerSettings.Page) then
			if (CastSequence.SequenceBuilder.GetActiveSlot() == activeToRemoveItem) then
				CastSequence.SequenceBuilder.Hide();
			end
			CastSequence.UnregisterSequence(activeToRemoveItem.Page, activeToRemoveItem.Slot);
			careerSettings.Page[activeToRemoveItem.Page].Slot[activeToRemoveItem.Slot] = nil;
			activeToRemoveItem = nil;
			SetupEntries();
		end
	end
	
end

function CastSequence.Builder.OnPopulate()

	if (not CastSequenceBuilderWindowList.PopulatorIndices) then
		return;
	end

	TintRows();
	
end

function CastSequence.Builder.Update(page, slot)
	
	SetupEntries();
	CastSequence.RegisterSequence(page, slot);

end

function CastSequence.Builder.OnNewLClick()
	
	CastSequence.SequenceBuilder.ShowNew();
	
end