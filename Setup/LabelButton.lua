CastSequenceLabelButton = Frame:Subclass("CastSequenceLabelButton");

local localization = CastSequence.Localization.GetMapping();

function CastSequenceLabelButton:Create(index, slot, page)

	local buttonName = "CastSequenceLabelButton" .. index;
	local button = self:CreateFromTemplate(buttonName, "Root");
	button.Name = buttonName;
	
	LabelSetTextColor(button.Name .. "Page", 140, 140, 140);
	WindowSetAlpha(button.Name .. "Background", 0.5);
	WindowSetTintColor(button.Name .. "Background", 0, 0, 255);
	
	button:Update(slot, page);
	
	return button;

end

function CastSequenceLabelButton:Update(slot, page)

	self.Slot = slot;
	self.Page = page;

	LabelSetText(self.Name .. "Slot", towstring(slot));
	LabelSetText(self.Name .. "Page", wstring.format(localization["LabelButton.Page"], page));
	
end

function CastSequenceLabelButton:SetShowing(showing)
	if (WindowGetShowing(self:GetName()) ~= showing) then
		WindowSetShowing(self:GetName(), showing);
	end
end

function CastSequenceLabelButton:AnchorTo(anchorFrame, pointOnAnchor, pointOnSelf, xOffset, yOffset)

	WindowClearAnchors(self:GetName());
	WindowAddAnchor(self:GetName(), pointOnAnchor, anchorFrame, pointOnSelf, xOffset, yOffset);

end

function CastSequenceLabelButton:OnLButtonUp(flags, mouseX, mouseY)
	CastSequence.SequenceBuilder.SetSlotFromHelp(self.Page, self.Slot)
end

function CastSequenceLabelButton:OnMouseOver(flags, mouseX, mouseY)
	WindowSetTintColor(self:GetName() .. "Background", 0, 255, 0);
end

function CastSequenceLabelButton:OnMouseOverEnd(flags, mouseX, mouseY)
	WindowSetTintColor(self:GetName() .. "Background", 0, 0, 255);
end