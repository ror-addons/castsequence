CastSequence = CastSequence or {};
CastSequence.SequenceBuilder = {};
CastSequence.SequenceBuilder.WindowName = "CastSequenceSequenceBuilderWindow";

local ActionType = { Ability = 1, Item = 2 };
local FRAME_WIDTH = 610;
local BUTTON_SIZE = 64;
local ACTIONBAR_PADDING = 10;
local ATTACHED_LIMIT = 10;
	
local windowName = CastSequence.SequenceBuilder.WindowName;
local localization = CastSequence.Localization.GetMapping();

local careerSettings = nil;
local buttons = {};
local sequence = { Count = 0, Slot = {} };
local firstVisible = 1;
local barSize = -1;
local helpButtons = nil;
local existingHotbar = {};

local function CreateSequenceItem(buttonData, attachedCount)
	
	local sequenceItem = {};	
	
	sequenceItem.Type = buttonData.Type;
	sequenceItem.Id = buttonData.Id;
	sequenceItem.Icon = buttonData.Icon;
	
	if (sequenceItem.Type == ActionType.Item) then
		sequenceItem.UniqueId = buttonData.UniqueId;
		sequenceItem.SpellId = buttonData.SpellId;
	end
	
	if (buttonData.Attached and (attachedCount or 0) <= ATTACHED_LIMIT) then
		sequenceItem.Attached = CreateSequenceItem(buttonData.Attached, (attachedCount or 0) + 1);
	end
	
	return sequenceItem;
		
end

local function BuildSequence()

	local sequenceName = TextEditBoxGetText(windowName .. "NameEditBox");
	local enabled = ButtonGetPressedFlag(windowName .. "EnabledCheckbox" .. "Button");
	local sequenceData = {};
	sequenceData.Name = sequenceName;
	sequenceData.Enabled = enabled;
	sequenceData.Action = {};
	sequenceData.Reset = {};
	
	if (ButtonGetPressedFlag(windowName .. "ResetTimeoutCheckbox" .. "Button")) then
		local timeout = TextEditBoxGetText(windowName .. "ResetTimeoutEditBox");
		if (timeout ~= L"" and tonumber(timeout) > 0) then
			sequenceData.Reset.Timeout = tonumber(timeout);
		end
	end
	
	if (ButtonGetPressedFlag(windowName .. "ResetOnTargetChangedCheckbox" .. "Button")) then
		local target = ComboBoxGetSelectedMenuItem(windowName .. "ResetOnTargetChangedComboBox");
		if (target > 0) then
			sequenceData.Reset.Target = target;
		end
	end
	
	if (ButtonGetPressedFlag(windowName .. "ResetInCombatChangedCheckbox" .. "Button")) then
		sequenceData.Reset.InCombatChanged = true;
	end
	
	for index = 1, sequence.Count do
		local buttonData = sequence.Slot[index];
		local sequenceItem = CreateSequenceItem(buttonData);
		
		table.insert(sequenceData.Action, sequenceItem);
	end
	
	return sequenceData;

end

local function ResizeActionBar(buttonCount)

	local barWidth, barHeight = WindowGetDimensions(windowName .. "ActionBar");
	local width, height = WindowGetDimensions(windowName .. "PreviousButton");
	local maxButtons = math.floor((FRAME_WIDTH - (width * 2)) / BUTTON_SIZE);
	
	buttonCount = math.min(math.max(1, tonumber(buttonCount)), maxButtons);

	if (barSize == buttonCount) then return end
	barSize = buttonCount;
	
	WindowSetDimensions(windowName .. "ActionBar", buttonCount * BUTTON_SIZE + ACTIONBAR_PADDING, barHeight);
	
	for index = 1, maxButtons do
		buttons[index]:SetShowing(index <= buttonCount);
	end

end

local function ShowButtons(showApply, showCreate)

	if (WindowGetShowing(windowName .. "ApplyButton") == showApply and WindowGetShowing(windowName .. "CreateButton") == showCreate) then return end

	WindowSetShowing(windowName .. "ApplyButton", showApply);
	WindowSetShowing(windowName .. "CreateButton", showCreate);
	
	if (showCreate) then
		WindowClearAnchors(windowName .. "CreateButton");
		if (showApply) then
			WindowAddAnchor(windowName .. "CreateButton", "topleft", windowName .. "ApplyButton", "topright", 0, 0);
		else
			WindowAddAnchor(windowName .. "CreateButton", "bottomright", windowName, "bottomright", -20, -20);
		end
	end

end

local function SetupError(name, text)

	WindowSetAlpha(windowName .. name, 0.7);
	WindowSetTintColor(windowName .. name, 255, 0, 0);
	LabelSetTextColor(windowName .. name .. "Label", 255, 0, 0);
	LabelSetText(windowName .. name .. "Label", text);

end

local function ShowError(name, visible)

	if (WindowGetShowing(windowName .. name) == visible) then return end

	WindowSetShowing(windowName .. name, visible);
	WindowSetShowing(windowName .. name .. "Label", visible);

end

local function HideErrors()

	ShowError("NameArrow", false);
	ShowError("PageArrow", false);
	ShowError("SlotArrow", false);
	ShowError("ActionBarArrow", false);
	
end

local function CreateButtonData(sequenceItem, attachedCount)
	
	local buttonData = {};
	buttonData.Type = sequenceItem.Type;
	buttonData.Id = sequenceItem.Id;
	buttonData.Icon = sequenceItem.Icon;
	
	if (sequenceItem.Attached and (attachedCount or 0) <= ATTACHED_LIMIT) then
		buttonData.Attached = CreateButtonData(sequenceItem.Attached, (attachedCount or 0) + 1);
	end
	
	if (sequenceItem.Type == ActionType.Ability) then
		local abilityData = GetAbilityData(sequenceItem.Id);
		buttonData.Name = abilityData.name;
	else
		buttonData.UniqueId = sequenceItem.UniqueId;
		buttonData.SpellId = sequenceItem.SpellId;
		local itemData = DataUtils.FindItem(sequenceItem.UniqueId);
		
		if (itemData) then
			buttonData.Name = itemData.name;
		end
	end
	
	return buttonData;
		
end

local function LoadSequence(page, slot)

	existingHotbar = { Page = page, Slot = slot };
	local sequenceSettings = nil;
	
	if (page and slot) then
		if (careerSettings.Page[page]) then
			sequenceSettings = careerSettings.Page[page].Slot[slot];
		end
	end
	
	sequence = { Count = 0, Slot = {} };
	
	if (existingHotbar.Page and existingHotbar.Slot) then
		TextEditBoxSetText(windowName .. "NameEditBox", sequenceSettings.Name);
		TextEditBoxSetText(windowName .. "PageEditBox", towstring(page));
		TextEditBoxSetText(windowName .. "SlotEditBox", towstring(slot));
		ButtonSetPressedFlag(windowName .. "EnabledCheckbox" .. "Button", (sequenceSettings.Enabled == true));
		
		for index, sequenceItem in ipairs(sequenceSettings.Action) do
			sequence.Count = sequence.Count + 1;
			sequence.Slot[index] = CreateButtonData(sequenceItem);
		end
		
		ButtonSetPressedFlag(windowName .. "ResetTimeoutCheckbox" .. "Button", (sequenceSettings.Reset.Timeout ~= nil));
		ButtonSetPressedFlag(windowName .. "ResetOnTargetChangedCheckbox" .. "Button", (sequenceSettings.Reset.Target ~= nil));
		ButtonSetPressedFlag(windowName .. "ResetInCombatChangedCheckbox" .. "Button", (sequenceSettings.Reset.InCombatChanged ~= nil));
		
		TextEditBoxSetText(windowName .. "ResetTimeoutEditBox", towstring(sequenceSettings.Reset.Timeout or ""));
		ComboBoxSetSelectedMenuItem(windowName .. "ResetOnTargetChangedComboBox", (sequenceSettings.Reset.Target or 0));
	else
		TextEditBoxSetText(windowName .. "NameEditBox", L"");
		TextEditBoxSetText(windowName .. "PageEditBox", L"");
		TextEditBoxSetText(windowName .. "SlotEditBox", L"");
		ButtonSetPressedFlag(windowName .. "EnabledCheckbox" .. "Button", true);
		
		ButtonSetPressedFlag(windowName .. "ResetTimeoutCheckbox" .. "Button", false);
		ButtonSetPressedFlag(windowName .. "ResetOnTargetChangedCheckbox" .. "Button", false);
		ButtonSetPressedFlag(windowName .. "ResetInCombatChangedCheckbox" .. "Button", false);
		
		TextEditBoxSetText(windowName .. "ResetTimeoutEditBox", L"");
		ComboBoxSetSelectedMenuItem(windowName .. "ResetOnTargetChangedComboBox", 0);
	end
	
	HideErrors();
	
	if (existingHotbar.Slot) then
		ShowButtons(true, false);
	else
		ShowButtons(false, true);
	end
	
	CastSequence.SequenceBuilder.DisplaySequence();

end

local function HasErrors()

	local hasError = false;
	local sequenceName = TextEditBoxGetText(windowName .. "NameEditBox");
	local barPage = TextEditBoxGetText(windowName .. "PageEditBox");
	local barSlot = TextEditBoxGetText(windowName .. "SlotEditBox");
	local slot = tonumber(barSlot);
	local page = tonumber(barPage);
	
	if (sequenceName == L"") then
		ShowError("NameArrow", true);
		hasError = true;
	else
		ShowError("NameArrow", false);
	end
	
	if (barPage == L"" or page < 1 or page > 10) then
		SetupError("PageArrow", wstring.format(localization["SequenceBuilder.Error.MustBeBetweenNumber"], 1, 10));
		ShowError("PageArrow", true);
		hasError = true;
	else
		ShowError("PageArrow", false);
	end
	
	if (barSlot == L"" or slot < 1 or slot > ActionBarConstants.BUTTONS) then
		SetupError("SlotArrow", wstring.format(localization["SequenceBuilder.Error.MustBeBetweenNumber"], 1, ActionBarConstants.BUTTONS));
		ShowError("SlotArrow", true);
		hasError = true;
	elseif (careerSettings.Page[page] and careerSettings.Page[page].Slot[slot] and 
		(existingHotbar.Slot == nil or (existingHotbar.Slot and existingHotbar.Slot ~= slot and existingHotbar.Page ~= page))) then
		
		SetupError("SlotArrow", localization["SequenceBuilder.Error.InUse"]);
		ShowError("SlotArrow", true);
		hasError = true;
	else
		ShowError("SlotArrow", false);
	end
	
	if (sequence.Count == 0) then
		ShowError("ActionBarArrow", true);
		hasError = true;
	else
		ShowError("ActionBarArrow", false);
	end
	
	return hasError;

end

function CastSequence.SequenceBuilder.Initialize()

	careerSettings = CastSequence.Settings.Career[GameData.Player.career.line];

	RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_PAGE_UPDATED, "CastSequence.SequenceBuilder.OnHotbarPageUpdated");
	
	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "center", "Root", "center", 0, 0);
			
	LabelSetText(windowName .. "TitleLabel", localization["SequenceBuilder.Title"]);
	LabelSetText(windowName .. "EnabledLabel", localization["SequenceBuilder.Enabled"]);
	LabelSetText(windowName .. "NameLabel", localization["SequenceBuilder.Name"]);
	LabelSetText(windowName .. "PageLabel", localization["SequenceBuilder.Page"]);
	LabelSetText(windowName .. "SlotLabel", localization["SequenceBuilder.Slot"]);
	LabelSetText(windowName .. "SlotHelpCheckBoxLabel", localization["SequenceBuilder.ShowHelp"]);
	
	LabelSetText(windowName .. "ResetLabel", localization["SequenceBuilder.ResetConditions"]);
	LabelSetText(windowName .. "ResetTimeoutCheckBoxLabel",localization["SequenceBuilder.ResetConditions.Timeout"]);
	LabelSetText(windowName .. "ResetOnTargetChangedCheckBoxLabel", localization["SequenceBuilder.ResetConditions.Target"]);
	LabelSetText(windowName .. "ResetTimeoutEditBoxLabel", localization["SequenceBuilder.ResetConditions.Timeout.Seconds"]);
	LabelSetText(windowName .. "ResetInCombatChangedCheckBoxLabel", localization["SequenceBuilder.ResetConditions.Combat"]);
	
	ComboBoxClearMenuItems(windowName .. "ResetOnTargetChangedComboBox");
	ComboBoxAddMenuItem(windowName .. "ResetOnTargetChangedComboBox", localization["SequenceBuilder.ResetConditions.Target.Friendly"]);
	ComboBoxAddMenuItem(windowName .. "ResetOnTargetChangedComboBox", localization["SequenceBuilder.ResetConditions.Target.Hostile"]);
	ComboBoxAddMenuItem(windowName .. "ResetOnTargetChangedComboBox", localization["SequenceBuilder.ResetConditions.Target.FriendlyOrHostile"]);
	
	SetupError("NameArrow", localization["SequenceBuilder.Error.Required"]);
	SetupError("PageArrow", wstring.format(localization["SequenceBuilder.Error.MustBeBetweenNumber"], 1, 10));
	SetupError("SlotArrow", wstring.format(localization["SequenceBuilder.Error.MustBeBetweenNumber"], 1, ActionBarConstants.BUTTONS));
	SetupError("ActionBarArrow", localization["SequenceBuilder.Error.AtLeastOneActionRequired"]);
	
	ButtonSetText(windowName .. "ApplyButton", localization["SequenceBuilder.Buttons.Apply"]);
	ButtonSetText(windowName .. "CreateButton", localization["SequenceBuilder.Buttons.Create"]);
	
	local barWidth, barHeight = WindowGetDimensions(windowName .. "ActionBar");
	local width, height = WindowGetDimensions(windowName .. "PreviousButton");
	local maxButtons = math.floor((FRAME_WIDTH - (width * 2)) / BUTTON_SIZE);
	
	for index = 1, maxButtons do
		buttons[index] = CastSequenceButton:Create(windowName .. "ActionBarButton" .. index, index, windowName .. "ActionBar", CastSequence.SequenceBuilder.UpdateSlot);
		
		if (index > 1) then
			buttons[index]:AnchorTo(buttons[index - 1]:GetName(), "right", "left", 0, 0);
		else
			buttons[index]:AnchorTo(windowName .. "ActionBar", "topleft", "topleft", 5, 5);
		end
		
		buttons[index]:SetShowing(true);
	end
	
    ButtonSetDisabledFlag(windowName .. "PreviousButton", true);
    ButtonSetDisabledFlag(windowName .. "NextButton", true);
	
	HideErrors();
	ResizeActionBar(1);
	ShowButtons(false, true);
	
	CastSequence.FindAbility.Initialize();
	
end

function CastSequence.SequenceBuilder.OnHotbarPageUpdated(physicalPage, logicalPage)

	if (not helpButtons) then return end
		
	local firstButtonSlot = 1;
	for bar = 1, CREATED_HOTBAR_COUNT  do
	
		local buttonCount = ActionBarClusterSettingsManager:GetActionBarSetting(bar, "buttonCount");
		
		if (bar == physicalPage) then
			local hotbar = ActionBars:GetBar(bar);
			local isVisible = WindowGetShowing(hotbar:GetName());
			if (isVisible) then				
				for slot = 1, buttonCount do
					local helpButton = helpButtons[firstButtonSlot + slot];					
					helpButton:Update(slot, logicalPage);
				end
			end
			
			return
		end
		
		firstButtonSlot = firstButtonSlot + buttonCount;		
	end
	
end

function CastSequence.SequenceBuilder.ShowNew()

	ButtonSetPressedFlag(windowName .. "ShowAttachedCheckbox" .. "Button", false);
	
	for index, button in ipairs(buttons) do
		button:ShowAttached(false);
	end	

	LoadSequence(nil);
	CastSequence.SequenceBuilder.Show();

end

function CastSequence.SequenceBuilder.ShowEdit(page, slot)

	ButtonSetPressedFlag(windowName .. "ShowAttachedCheckbox" .. "Button", false);
	
	for index, button in ipairs(buttons) do
		button:ShowAttached(false);
	end	

	LoadSequence(page, slot);
	CastSequence.SequenceBuilder.Show();

end

function CastSequence.SequenceBuilder.GetActiveSlot()
	return existingHotbar;
end

function CastSequence.SequenceBuilder.Show()

	if (WindowGetShowing(windowName)) then return end
	
	-- if the builder menu hasn't been moved, reposition the windows so that the sequence is in the center
	-- of the screen
	local x, y = WindowGetScreenPosition(CastSequence.Builder.WindowName);
	if (x == CastSequence.Builder.DefaultPosition.X and y == CastSequence.Builder.DefaultPosition.Y) then
		local width, height = WindowGetDimensions(windowName);
		WindowClearAnchors(CastSequence.Builder.WindowName);
		WindowAddAnchor(CastSequence.Builder.WindowName, "center", "Root", "topright", -(width / 2), -(height / 2));
	end
	
	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "topright", CastSequence.Builder.WindowName, "topleft", 0, 0);
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, CastSequence.Setup.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function CastSequence.SequenceBuilder.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function CastSequence.SequenceBuilder.OnHidden()

	CastSequence.FindAbility.Hide();
	ButtonSetPressedFlag(windowName .. "SlotHelpCheckbox" .. "Button", false);
	CastSequence.SequenceBuilder.ShowHelp(false);

end

function CastSequence.SequenceBuilder.OnCloseLUp()

	CastSequence.SequenceBuilder.Hide();

end

function CastSequence.SequenceBuilder.OnEnabledLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "EnabledCheckbox" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "EnabledCheckbox" .. "Button", isChecked);
	
end

function CastSequence.SequenceBuilder.OnShowAttachedLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ShowAttachedCheckbox" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "ShowAttachedCheckbox" .. "Button", isChecked);
	
	for index, button in ipairs(buttons) do
		button:ShowAttached(isChecked);
	end	
	
end

function CastSequence.SequenceBuilder.OnShowAttachedMouseOver()

	local windowName = SystemData.ActiveWindow.name;
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, localization["SequenceBuilder.ShowAttached.Tooltip"]);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "bottomleft", XOffset = 0, YOffset = -5 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
	
end

function CastSequence.SequenceBuilder.OnFindAbilityLUp()
	CastSequence.FindAbility.Show();
end

function CastSequence.SequenceBuilder.OnFindAbilityMouseOver()

	local windowName = SystemData.ActiveWindow.name;
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, localization["SequenceBuilder.FindAbility.Tooltip"]);
	--Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "bottomleft", XOffset = 0, YOffset = -5 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
	
end

function CastSequence.SequenceBuilder.OnPageChanged()

	CastSequence.SequenceBuilder.OnSlotChanged();

end

function CastSequence.SequenceBuilder.OnSlotChanged()

	local barPage = TextEditBoxGetText(windowName .. "PageEditBox");
	local barSlot = TextEditBoxGetText(windowName .. "SlotEditBox");
	local slot = tonumber(barSlot);
	local page = tonumber(barPage);

	if (barSlot == L"" or barPage == L"") then return end

	ShowError("SlotArrow", false);
			
	if (existingHotbar.Slot) then
		if (existingHotbar.Slot ~= slot or existingHotbar.Page ~= page) then
			ShowButtons(true, true);
	
			if (careerSettings.Page[page] and careerSettings.Page[page].Slot[slot]) then
				SetupError("SlotArrow", localization["SequenceBuilder.Error.InUse"]);
				ShowError("SlotArrow", true);
			end
		else
			ShowButtons(true, false);
		end
	end

end

function CastSequence.SequenceBuilder.OnPreviousButtonLUp()
	if (firstVisible > 1) then
		firstVisible = firstVisible - 1;
		CastSequence.SequenceBuilder.DisplaySequence();
	end
end

function CastSequence.SequenceBuilder.OnNextButtonLUp()
	if (sequence.Count + 1 > firstVisible + #buttons - 1) then
		firstVisible = firstVisible + 1;
		CastSequence.SequenceBuilder.DisplaySequence();
	end
end

function CastSequence.SequenceBuilder.OnSlotHelpLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "SlotHelpCheckbox" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "SlotHelpCheckbox" .. "Button", isChecked);
	
	CastSequence.SequenceBuilder.ShowHelp(isChecked);
	
end

function CastSequence.SequenceBuilder.OnResetTimeoutLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ResetTimeoutCheckbox" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "ResetTimeoutCheckbox" .. "Button", isChecked);
	
end

function CastSequence.SequenceBuilder.OnResetOnTargetChangedLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ResetOnTargetChangedCheckbox" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "ResetOnTargetChangedCheckbox" .. "Button", isChecked);
	
end

function CastSequence.SequenceBuilder.OnResetInCombatChangedLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ResetInCombatChangedCheckbox" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "ResetInCombatChangedCheckbox" .. "Button", isChecked);
	
end

function CastSequence.SequenceBuilder.DisplaySequence()
	
	ResizeActionBar(sequence.Count + 1);
	
	if (firstVisible > 1) then
		if (firstVisible + #buttons - 1 > sequence.Count + 1) then
			firstVisible = math.max(1, sequence.Count + 1 - (#buttons - 1));
		end
	end

	for index, button in ipairs(buttons) do
		local buttonData = sequence.Slot[firstVisible + index - 1];
		if (button.ButtonData ~= buttonData) then
			button:SetButtonData(buttonData);
		end
	end
	
    ButtonSetDisabledFlag(windowName .. "PreviousButton", (firstVisible == 1));
    ButtonSetDisabledFlag(windowName .. "NextButton", (sequence.Count < firstVisible + #buttons - 1));

end

function CastSequence.SequenceBuilder.UpdateSlot(index, buttonData)

	local slot = firstVisible + index - 1;

	if (buttonData) then
		sequence.Slot[slot] = buttonData;		
		if (slot > sequence.Count) then
			sequence.Count = sequence.Count + 1;
		end		
	else
		if (slot <= sequence.Count) then
			for slotIndex = slot, sequence.Count - 1 do
				sequence.Slot[slotIndex] = sequence.Slot[slotIndex + 1];
			end
			sequence.Slot[sequence.Count] = nil;
			sequence.Count = sequence.Count - 1;
		end
	end
	
	CastSequence.SequenceBuilder.DisplaySequence();

end

function CastSequence.SequenceBuilder.SetSlotFromHelp(page, slot)
	CastSequence.SequenceBuilder.ShowHelp(false);
	TextEditBoxSetText(windowName .. "PageEditBox", towstring(page));
	TextEditBoxSetText(windowName .. "SlotEditBox", towstring(slot));
	ButtonSetPressedFlag(windowName .. "SlotHelpCheckbox" .. "Button", false);
end

function CastSequence.SequenceBuilder.DestroyHelp()
	
	if (not helpButtons) then return end;
	
	for _, helpButton in pairs(helpButtons) do
		helpButton:Destroy();
	end
	
	helpButtons = nil;
		
end

function CastSequence.SequenceBuilder.ShowHelp(visible)

	if (helpButtons) then
		for _, helpButton in pairs(helpButtons) do
			helpButton:SetShowing(visible);
		end
		return;
	end
		
	if (visible and not helpButtons) then
		helpButtons = {};
		
		local firstButtonSlot = 1;
		for bar = 1, CREATED_HOTBAR_COUNT  do
			local buttonCount = ActionBarClusterSettingsManager:GetActionBarSetting(bar, "buttonCount");
			local hotbar = ActionBars:GetBar(bar);
			local isVisible = WindowGetShowing(hotbar:GetName());
			local page = GetHotbarPage(bar);
			
			if (isVisible) then
				for slot = 1, buttonCount do
					local helpButton = CastSequenceLabelButton:Create(firstButtonSlot + slot, slot, page);
					local hotbarButton = hotbar.m_Buttons[slot];
					
					WindowSetScale(helpButton:GetName(), WindowGetScale(hotbarButton:GetName()));
					WindowSetLayer(helpButton:GetName(), 3);
					
					helpButton:AnchorTo(hotbarButton:GetName(), "topleft", "topleft", 0, 0);
					helpButton:SetShowing(isVisible);
					
					helpButtons[firstButtonSlot + slot] = helpButton;
				end
			end
			
			firstButtonSlot = firstButtonSlot + buttonCount;		
		end
		
	end
	
end

function CastSequence.SequenceBuilder.OnCreateLClick()

	if (HasErrors()) then
		return;
	end

	local barPage = TextEditBoxGetText(windowName .. "PageEditBox");
	local barSlot = TextEditBoxGetText(windowName .. "SlotEditBox");
	local slot = tonumber(barSlot);
	local page = tonumber(barPage);
	
	careerSettings.Page[page] = careerSettings.Page[page] or { Slot = {} };
	careerSettings.Page[page].Slot[slot] = BuildSequence();

	CastSequence.SequenceBuilder.Hide();
	CastSequence.Builder.Update(page, slot);

end

function CastSequence.SequenceBuilder.OnApplyLClick()

	if (HasErrors()) then
		return;
	end

	local barPage = TextEditBoxGetText(windowName .. "PageEditBox");
	local barSlot = TextEditBoxGetText(windowName .. "SlotEditBox");
	local slot = tonumber(barSlot);
	local page = tonumber(barPage);
	
	if (slot ~= existingHotbar.Slot or page ~= existingHotbar.Page) then
		CastSequence.UnregisterSequence(existingHotbar.Page, existingHotbar.Slot);
		if (careerSettings.Page[existingHotbar.Page] and careerSettings.Page[existingHotbar.Page].Slot) then
			careerSettings.Page[existingHotbar.Page].Slot[existingHotbar.Slot] = nil;
		end
	end
	
	careerSettings.Page[page] = careerSettings.Page[page] or { Slot = {} };
	careerSettings.Page[page].Slot[slot] = BuildSequence();

	CastSequence.SequenceBuilder.Hide();
	CastSequence.Builder.Update(page, slot);

end